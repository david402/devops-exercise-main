# DevOps/Systems Exercise

[DevOps Frontend Example App](https://gitlab.com/campusedu/public/devops-exercise-frontend)

[DevOps Backend Example App](https://gitlab.com/campusedu/public/devops-exercise-backend)


## Objective
Create a way to deploy our example app using the above frontend project, the above backend project, and a postgres (v13) database. The objective is to evaluate your ability to understand projects at a high level and get them running together.

## Deliverable
Please email or submit a merge request on this repo providing your solution. **The solution can be in any form you are most comfortable.** This can be any combination of documented manual steps, script file(s) (shell/powershell), programming (Python, Ansible, Terraform, etc.) and/or other infrastructure tools (Virtualbox, Vagrant, Docker, Kubernetes, etc). The goal is create a documented, repeatable pattern for deploying the example app.

Feel free to ask questions or even open issues on this and/or related repos if you find an issue or feels something needs more clarity.












